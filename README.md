# LoadENVT
Read ENVT files with Athena

## Get package and very first setup
```
git clone https://gitlab.cern.ch/ozaplati/LoadENVT.git

cd LoadENVT/build
asetup AthAnalysis,21.2.39,here
cmake ../source
make
```


## Setup
```
cd LoadENVT
source my_setup.sh
```

## Download a testing file from rucio
### Download a testting file:
```
setupATLAS
voms-proxi-init -voms atlas
lsetup rucio

rucio list-dids mc15_13TeV:mc15_13TeV.3679*.Pythia8EvtGen_A14NNPDF23LO_jetjet_eta30*EVNT*e8466
```
- choose one dids, e.q.: `mc15_13TeV:mc15_13TeV.367918.Pythia8EvtGen_A14NNPDF23LO_jetjet_eta30_JZ3WithSW.evgen.EVNT.e8466`
```
rucio list-files mc15_13TeV:mc15_13TeV.367918.Pythia8EvtGen_A14NNPDF23LO_jetjet_eta30_JZ3WithSW.evgen.EVNT.e8466
```
- choose one file e.q.: `mc15_13TeV:EVNT.29433025._000250.pool.root.1`
```
rucio -v download mc15_13TeV:EVNT.29433025._000250.pool.root.1
```

### Update JobOption
- use the testing file as `FilesInput` in JobOption `source/MyPackage/share/AlgJobOptions_Jet.py`
- update lines:
```
jps.AthenaCommonFlags.FilesInput = [
"<PATH>/EVNT.29433025._000250.pool.root.1",
]
```

## Local run
```
cd LoadEVNT/run
source run_local.sh
```
note, `run/README` includes some very old notes, some of them may not work correctly, it should be tested. There are also some notes to run in batch/grid.
