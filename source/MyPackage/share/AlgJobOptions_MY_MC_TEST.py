#Skeleton joboption for a simple analysis job

#---- Minimal job options -----

jps.AthenaCommonFlags.AccessMode = "POOLAccess"              #Choose from TreeAccess,BranchAccess,ClassAccess,AthenaAccess,POOLAccess
#jps.AthenaCommonFlags.TreeName = "MyTree"                    #when using TreeAccess, must specify the input tree name

jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:newIncludeInJB_pions100Evt.root"]

athAlgSeq += CfgMgr.MyPackageAlg()                               #adds an instance of your alg to the main alg sequence


#---- Options you could specify on command line -----
jps.AthenaCommonFlags.EvtMax=-1                          #set on command-line with: --evtMax=-1
#jps.AthenaCommonFlags.SkipEvents=0                       #set on command-line with: --skipEvents=0
jps.AthenaCommonFlags.FilesInput = [
#"/eos/home-o/ozaplati/Validation/test_MY_MC_pions/EVNTFile_piplus_eta49_test.root"
#"/eos/home-o/ozaplati/Validation/test_piplus/user.ozaplati.singleParticleSample13TeV.MC15.999999.ParticleGun_piplus_eta22_23_1000Evt_from_terminal_with_split_vol2_EXT0/user.ozaplati.21207244.EXT0._000001.ota.pool.root"
#"/eos/home-o/ozaplati/Validation/test_piplus/test_only_split_include/user.ozaplati.singleParticleSample13TeV.MC15.999999.ParticleGun_piplus_eta22_23_1000Evt_from_terminal_only_split_include_EXT0/user.ozaplati.21208419.EXT0._000001.ota.pool.root"
#"/eos/home-o/ozaplati/Validation/test_piplus/test_nEventPerJon_and_nJobs/user.ozaplati.singleParticleSample13TeV.MC15.999999.ParticleGun_piplus_eta22_23_1500_use_nEventPerJob_and_nJobs_EXT0/user.ozaplati.21212335.EXT0._000001.ota.pool.root"
"/afs/cern.ch/work/o/ozaplati/public/SingleParticles/SingleParticles/ParticleGunGeneration/Output_ENVT/test_newIncludeJB_piplus_eta02_03.root"
]



include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above

