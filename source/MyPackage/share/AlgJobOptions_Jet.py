

#Skeleton joboption for a simple analysis job

#---- Minimal job options -----

jps.AthenaCommonFlags.AccessMode = "POOLAccess"           #Choose from TreeAccess,BranchAccess,ClassAccess,AthenaAccess,POOLAccess
jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:Jet.root"]

athAlgSeq += CfgMgr.MyPackageAlg()                        #adds an instance of your alg to the main alg sequence

jps.AthenaCommonFlags.EvtMax=100                           #set on command-line with: --evtMax=-1
#jps.AthenaCommonFlags.SkipEvents=0                       #set on command-line with: --skipEvents=0
jps.AthenaCommonFlags.FilesInput = [
"/eos/home-o/ozaplati/FOR_TANCREDI/EVNT/Pythia8EvtGen_A14NNPDF23LO_jetjet_eta30_EVNT_e8466/mc15_13TeV/EVNT.29433025._000248.pool.root.1",
]

include("AthAnalysisBaseComps/SuppressLogging.py")       #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above

