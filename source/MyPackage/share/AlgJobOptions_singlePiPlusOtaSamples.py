#Skeleton joboption for a simple analysis job

#---- Minimal job options -----

jps.AthenaCommonFlags.AccessMode = "POOLAccess"              #Choose from TreeAccess,BranchAccess,ClassAccess,AthenaAccess,POOLAccess
#jps.AthenaCommonFlags.TreeName = "MyTree"                    #when using TreeAccess, must specify the input tree name

jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:singlePiPlus_eta_02_03.root"]

athAlgSeq += CfgMgr.MyPackageAlg()                               #adds an instance of your alg to the main alg sequence


#---- Options you could specify on command line -----
jps.AthenaCommonFlags.EvtMax=-1                          #set on command-line with: --evtMax=-1
#jps.AthenaCommonFlags.SkipEvents=0                       #set on command-line with: --skipEvents=0
jps.AthenaCommonFlags.FilesInput = [
   #"/eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/user.ozaplati.singleParticleSample13TeV.MC15.999999.ParticleGun_single_piplus_logPt1to100_eta50_EXT0/user.ozaplati.20375653.EXT0._000002.ota.pool.root"
    "/eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/ENVT/single_piplus_eta_02_03.root"
]


include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above

