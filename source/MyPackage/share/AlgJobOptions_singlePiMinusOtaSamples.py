#Skeleton joboption for a simple analysis job

#---- Minimal job options -----

jps.AthenaCommonFlags.AccessMode = "POOLAccess"              #Choose from TreeAccess,BranchAccess,ClassAccess,AthenaAccess,POOLAccess
#jps.AthenaCommonFlags.TreeName = "MyTree"                    #when using TreeAccess, must specify the input tree name

jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:singlePiMinus_OtaSamples.root"]

athAlgSeq += CfgMgr.MyPackageAlg()                               #adds an instance of your alg to the main alg sequence


#---- Options you could specify on command line -----
jps.AthenaCommonFlags.EvtMax=-1                          #set on command-line with: --evtMax=-1
#jps.AthenaCommonFlags.SkipEvents=0                       #set on command-line with: --skipEvents=0
jps.AthenaCommonFlags.FilesInput = [
   "/eos/home-o/ozaplati/Validation/SingleParticleSamples/piminus/user.ozaplati.singleParticleSample13TeV.MC15.999998.ParticleGun_single_piminus_logPt1to100_eta50_EXT0/user.ozaplati.20387528.EXT0._000001.ota.pool.root",
   "/eos/home-o/ozaplati/Validation/SingleParticleSamples/piminus/user.ozaplati.singleParticleSample13TeV.MC15.999998.ParticleGun_single_piminus_logPt1to100_eta50_EXT0/user.ozaplati.20387528.EXT0._000002.ota.pool.root",
   "/eos/home-o/ozaplati/Validation/SingleParticleSamples/piminus/user.ozaplati.singleParticleSample13TeV.MC15.999998.ParticleGun_single_piminus_logPt1to100_eta50_EXT0/user.ozaplati.20387528.EXT0._000003.ota.pool.root",
   "/eos/home-o/ozaplati/Validation/SingleParticleSamples/piminus/user.ozaplati.singleParticleSample13TeV.MC15.999998.ParticleGun_single_piminus_logPt1to100_eta50_EXT0/user.ozaplati.20387528.EXT0._000004.ota.pool.root",
   "/eos/home-o/ozaplati/Validation/SingleParticleSamples/piminus/user.ozaplati.singleParticleSample13TeV.MC15.999998.ParticleGun_single_piminus_logPt1to100_eta50_EXT0/user.ozaplati.20387528.EXT0._000005.ota.pool.root",
   "/eos/home-o/ozaplati/Validation/SingleParticleSamples/piminus/user.ozaplati.singleParticleSample13TeV.MC15.999998.ParticleGun_single_piminus_logPt1to100_eta50_EXT0/user.ozaplati.20387528.EXT0._000006.ota.pool.root",
   "/eos/home-o/ozaplati/Validation/SingleParticleSamples/piminus/user.ozaplati.singleParticleSample13TeV.MC15.999998.ParticleGun_single_piminus_logPt1to100_eta50_EXT0/user.ozaplati.20387528.EXT0._000007.ota.pool.root",
   "/eos/home-o/ozaplati/Validation/SingleParticleSamples/piminus/user.ozaplati.singleParticleSample13TeV.MC15.999998.ParticleGun_single_piminus_logPt1to100_eta50_EXT0/user.ozaplati.20387528.EXT0._000008.ota.pool.root",
   "/eos/home-o/ozaplati/Validation/SingleParticleSamples/piminus/user.ozaplati.singleParticleSample13TeV.MC15.999998.ParticleGun_single_piminus_logPt1to100_eta50_EXT0/user.ozaplati.20387528.EXT0._000009.ota.pool.root",
   "/eos/home-o/ozaplati/Validation/SingleParticleSamples/piminus/user.ozaplati.singleParticleSample13TeV.MC15.999998.ParticleGun_single_piminus_logPt1to100_eta50_EXT0/user.ozaplati.20387528.EXT0._000010.ota.pool.root",
]


include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above

