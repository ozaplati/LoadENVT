#Skeleton joboption for a simple analysis job

#---- Minimal job options -----

jps.AthenaCommonFlags.AccessMode = "POOLAccess"              #Choose from TreeAccess,BranchAccess,ClassAccess,AthenaAccess,POOLAccess
#jps.AthenaCommonFlags.TreeName = "MyTree"                    #when using TreeAccess, must specify the input tree name

jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:singleParticle_singlePhoton.root"]

athAlgSeq += CfgMgr.MyPackageAlg()                               #adds an instance of your alg to the main alg sequence


#---- Options you could specify on command line -----
jps.AthenaCommonFlags.EvtMax=-1                          #set on command-line with: --evtMax=-1
#jps.AthenaCommonFlags.SkipEvents=0                       #set on command-line with: --skipEvents=0
jps.AthenaCommonFlags.FilesInput = [
	"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423000.ParticleGun_single_electron_egammaET.evgen.EVNT.e5112/EVNT.hadded.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000001.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000002.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000003.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000004.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000005.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000006.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000007.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000008.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000009.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000010.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000011.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000012.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000013.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000014.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000015.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000016.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000017.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000018.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000019.pool.root.1",
	#"/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112/EVNT.08549531._000020.pool.root.1",
]        #set on command-line with: --filesInput=...


include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above

