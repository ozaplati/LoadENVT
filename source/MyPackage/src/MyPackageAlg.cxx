// MyPackage includes
#include "MyPackageAlg.h"
#include "GeneratorObjects/McEventCollection.h"


//#include "xAODEventInfo/EventInfo.h"




MyPackageAlg::MyPackageAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
  
}


MyPackageAlg::~MyPackageAlg() {}


StatusCode MyPackageAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //
  m_myHistPt = new TH1D("myHistPt","myHistPt",1000,0,500000);
  CHECK( histSvc()->regHist("/MYSTREAM/myHistPt", m_myHistPt) ); //registers histogram to output stream
  
  

  m_myHistEta = new TH1D("myHistEta","myHistEta",100,-5.0,5.0);
  CHECK( histSvc()->regHist("/MYSTREAM/myHistEta",m_myHistEta) );
  
  //m_myTree = new TTree("myTree","myTree");
  //CHECK( histSvc()->regTree("/MYSTREAM/SubDirectory/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory


  return StatusCode::SUCCESS;
}

StatusCode MyPackageAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  

  m_myHistPt->GetXaxis()->SetTitle("single particle pt [MeV]");
  m_myHistPt->GetYaxis()->SetTitle("N [-]");

  m_myHistEta->GetXaxis()->SetTitle("single particle #eta [-]");
  m_myHistEta->GetYaxis()->SetTitle("N [-]");

  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}




StatusCode MyPackageAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  
  setFilterPassed(false); //optional: start with algorithm not passed



  //
  //Your main analysis code goes here
  //If you will use this algorithm to perform event skimming, you
  //should ensure the setFilterPassed method is called
  //If never called, the algorithm is assumed to have 'passed' by default
  //


  //HERE IS AN EXAMPLE
  //const xAOD::EventInfo* ei = 0;
  //CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
  //ATH_MSG_INFO("eventNumber=" << ei->eventNumber() );
  //m_myHist->Fill( ei->averageInteractionsPerCrossing() ); //fill mu into histogram
 const McEventCollection* mymccoll;
  CHECK(evtStore()->retrieve(mymccoll, "GEN_EVENT"));
  for (unsigned int cntr = 0; cntr < mymccoll->size(); ++cntr)
    {
      const HepMC::GenEvent* ev_in = (*mymccoll)[cntr];
      if (!ev_in) continue;
      for (HepMC::GenEvent::particle_const_iterator itrPart = ev_in->particles_begin(); itrPart!=ev_in->particles_end();++itrPart)
        {
          if ( (*itrPart) )
            {
              std::cout << "ZH Particle: " << (*itrPart)->pdg_id() << " pT: " << (*itrPart)->momentum().perp() << " eta: " << (*itrPart)->momentum().eta() << std::endl;
              double pt = (*itrPart)->momentum().perp();
              double eta = (*itrPart)->momentum().eta();
              m_myHistPt->Fill( pt );
              m_myHistEta->Fill( eta );
            }
        }
    } // Loop over particles


  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode MyPackageAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}


