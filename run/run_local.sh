OUTDIR=Test_Local
JO=AlgJobOptions_Jet.py
DIR=${PWD}

# make output directory
mkdir -p ${OUTDIR}
cd ${OUTDIR}
# run athena with JO
athena ../source/MyPackage/share/${JO}

cd ${DIR}
