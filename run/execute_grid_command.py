# Single Particle grid run cript
import os, sys

#
# Setup part
#
user = "ozaplati"					# Grid user
sample_desc = "single_piplus"		# sample description:
									#		"single_electrons"
									# 		"single_photons"


#
# Call pathena for your sample_desc
#

if sample_desc == "single_electrons":
	rucio_sample = "valid1.423000.ParticleGun_single_electron_egammaET.evgen.EVNT.e5112"
	command = "pathena MyPackage/MyPackageAlgJobOptions.py --inDS=" + rucio_sample + " --outDS=user." + user + "." + sample_desc + " --mergeOutput"
	print (command)
	os.system(command)
	
elif sample_desc == "single_photons":
	rucio_sample = "valid1.423001.ParticleGun_single_photon_egammaET.evgen.EVNT.e5112"
	command = "pathena MyPackage/MyPackageAlgJobOptions.py --inDS=" + rucio_sample + " --outDS=user." + user + "." + sample_desc + " --mergeOutput"
	print(command)
	os.system(command)
elif sample_desc == "single_piplus":
	rucio_sample = "user.ozaplati.singleParticleSample13TeV.MC15.999999.PG_piplus_absEta_02_07_lowP_Barrel_5MEvt_042121_EXT0"
	command = "pathena MyPackage/MyPackageAlgJobOptions.py --inDS=" + rucio_sample + " --outDS=user." + user + "." + sample_desc + " --mergeOutput"
	print(command)
	os.system(command)
	
else:
	print("!!!! ATTANTION !!!!")
	print("     sample_desc: " + sample_desc + "is not defined in the script: execute_grid_commnd.py")
	print("     END WITHOUT ANY GRID RUN")
