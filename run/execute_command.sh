#!/bin/bash
#list_inputSampes='ListOfROOTFiles_singlePhotons'
list_inputSampes='ListOfROOTFiles_singlePiplus'
files_input=$(cat  ${list_inputSampes} |tr "\n" " ")
for inputFile in $(cat ${list_inputSampes})
	do
		echo ${inputFile}
		files_input+=" "
		files_input+=${inputFile}
	done

echo ${files_input}

athena MyPackage/MyPackageAlgJobOptions.py --filesInput=${files_input} --evtMax=900 


#athena MyPackage/MyPackageAlgJobOptions.py --filesInput=/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423000.ParticleGun_single_electron_egammaET.evgen.EVNT.e5112/EVNT.hadded.pool.root.1 --evtMax=-1 
#athena MyPackage/MyPackageAlgJobOptions.py --filesInput=/eos/home-o/ozaplati/Validation/SingleParticleSamples/valid1.423000.ParticleGun_single_electron_egammaET.evgen.EVNT.e5112/EVNT.08549528._000020.pool.root.1 --evtMax=-1 

